$(document).ready(function() 
{
    
    
    
    var introAnimation = new TimelineMax({paused: true, onComplete:introAnimationDone, repeat:0, repeatDelay:0, yoyo:false});
    
    introAnimation.fromTo('.alpha', 1, {autoAlpha:0, ease:Power2.easeInOut},{autoAlpha:1, ease:Power2.easeInOut}, 1)
    
    .fromTo('.line', .4, {left: -20, autoAlpha:0, ease:Power3.easeIn},{left: 0, autoAlpha:1, ease:Power2.easeOut}, "+=.6")
    
    .fromTo('.hero_image', .8, {autoAlpha:0, ease:Power3.easeOut},{autoAlpha:1, ease:Power3.easeInOut}, "+=.6")
    
    .fromTo('.cta_hero', .3, {autoAlpha:0, ease:Power2.easeOut},{autoAlpha:1, ease:Power2.easeIn}, "+=.3");
    
    //.fromTo('.title', .8, {autoAlpha:0, ease:Power3.easeOut},{autoAlpha:1, ease:Power3.easeInOut}, "+=2");
    
    
    function introAnimationDone(){}
    
    

    
    var _run = false;
    var _hover = false;
    var _offset = (!isMobile())? 40:50;
    var _open = false;
    
    
    window.onscroll = function (event) {}
    
    
    $('.logo_con').bind('click',function(event){
      TweenMax.to(window, 2, {scrollTo:{y:0}, ease:Power2.easeOut});
      event.preventDefault();
    });
 
    

    $('.nav_mobile').bind('click',function(event){
        event.preventDefault();
        
        if(!_open){
              open_mobile_nav();
           }else{
              close_mobile_nav();
           }

    });
    
    
    
    
    
    $('.close').bind('click',function(event){
        event.preventDefault();
        close_mobile_nav();
    });
    
    

     window.onresize = function (event) {}
    
     window.onload = function (event) {
       init();
     }
    
    
    function isMobile(){
        var _v = $(window).width() < 900;
        return _v;
    }
    
    $('.upload_con').onmouseover = function(){
        console.log("rolled");
        TweenMax.to(".folder_icon", .2, {css:{opacity:0}});
    }
    
    
    $('.jumplink').bind('click',function(event){
        var tarID = "#"+$(this).attr('rel');
        
        if(_open){
            close_mobile_nav();
        }
        
        var tar = ($(tarID).offset().top - _offset);
        TweenMax.to(window, 1, {scrollTo:{y:tar,autoKill:false}, ease:Power4.easeInOut});
        event.preventDefault();
    });
    
    
    $('.paylink').bind('click',function(event){
        open_payment();
        event.preventDefault();
    });
    
    
    
    
    
    function close_mobile_nav(){
         TweenMax.to(mobile_nav, .2, {autoAlpha: 0, ease:Power2.easeOut});
        $('.nav_mobile').html('<img src="img/menu.svg" class="menu_icon">');
        _open = false;
    }
    
    function open_mobile_nav(){
        $('.nav_mobile').html('<img src="img/menu_close.svg" class="menu_icon">');
         TweenMax.to(mobile_nav, .2, {autoAlpha: 1, ease:Power2.easeOut});
        _open = true;
    }
     
    
    
    function open_payment(){
        if(_open){
            close_mobile_nav();
        }
        TweenMax.to(payment, .2, {autoAlpha: 1, ease:Power2.easeOut});
    }
    function close_payment(){
        TweenMax.to(payment, .2, {autoAlpha: 0, ease:Power2.easeOut});
    }
    
    
    
    
    
    function init(){
        introAnimation.restart();
    }
    
    
    
    
    document.getElementById("file_upload").onchange = function () {
        document.getElementById("selected_files").value = this.value;
    };

    var inputs = document.querySelectorAll( '.upload_input.hidden' );
    Array.prototype.forEach.call( inputs, function( input ) {
        var label	 = document.getElementById("selected_files"),
            labelVal = label.innerHTML;
            input.addEventListener( 'change', function( e ) {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();
            if( fileName )
                label.value = fileName;
            else
                label.value = labelVal;
        });
    });

    $(document).ready(function() {
        $("input").on("focus", function () {
            $(this).removeClass("error");   
        });
    });	

    // var allowed_file_size = "26214400"; 25mb
    // var allowed_file_size = "10485760"; // 10 mb
    var allowed_file_size = "16777216"; // 16 mb
    var allowed_files = ['image/png', 'application/pdf', 'image/jpeg', 'image/jpg', 'image/pjpeg'];

    $("#getaquote").submit(function(e){
        e.preventDefault(); //prevent default action 
        proceed = true;

        //simple input validation
        $($(this).find("input[data-required=true], textarea[data-required=true]")).each(function(){
                if(!$.trim($(this).val())){ //if this field is empty 
                    $(this).addClass("error");   
                    proceed = false; //set do not proceed flag
                }
                //check invalid email
                var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
                if($(this).attr("type")=="email" && !email_reg.test($.trim($(this).val()))){
                    $(this).addClass("error");   
                    proceed = false; //set do not proceed flag              
                }   
        }).on("input", function(){ //change border color to original
            $(this).removeClass("error");   
        });

        //check file size and type before upload, works in modern browsers
        if(window.File && window.FileReader && window.FileList && window.Blob){
            var total_files_size = 0;
            $(this.elements['file_attach[]'].files).each(function(i, ifile){
                if(ifile.value !== ""){ //continue only if file(s) are selected
                    if(allowed_files.indexOf(ifile.type) === -1){ //check unsupported file
                        alert( ifile.name + " is unsupported file type. Supported types: png, jpg or pdf.");
                        proceed = false;
                    }
                 total_files_size = total_files_size + ifile.size; //add file size to total size
                }
            }); 
           if(total_files_size > allowed_file_size){ 
                alert( "Sorry, attachments have to be less than 16 MB.");
                proceed = false;
            }
        }

        //if everything's ok, continue with Ajax form submit
        if(proceed){ 
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = new FormData(this); //Creates new FormData object

            $.ajax({ //ajax form submit
                url : post_url,
                type: request_method,
                data : form_data,
                dataType : "json",
                contentType: false,
                cache: false,
                processData:false
            }).done(function(res){ //fetch server "json" messages when done
                if(res.type == "error"){
                    $("#contact_results").html('<div class="error">'+ res.text +"</div>");
                }

                if(res.type == "done"){
                    
                    $("#contact_results").html('<div class="success">'+ res.text +"</div>");
                    
                    TweenMax.to(getaquote, .7, {x:-200, autoAlpha:0, ease:Power3.easeOut});
                    
                    TweenMax.to(getaquote_title, .5, {opacity:0, ease:Power3.easeOut});
                    
                    TweenMax.to(contact_results, .8, {display:'flex', opacity:1, delay: .6, ease:Power3.easeOut});
                    
                }
            });
        }
    });
    
    
    
    
    
//END
});