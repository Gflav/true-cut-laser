$(document).ready(function() 
{

    
    var lightbox_html = '<!-- lightbox --><div class="lightbox"><div class="lightboxContent"></div></div><!-- END lightbox -->';
    
    var bodyScroll = 0;
    
    var _gallery = [];
    var _path = "";
    var _count = 0;
    
  $('body').append(lightbox_html);
    
    
    /*
    skel.viewport({
        width: "device-width",
        scalable: false
    });
    */
    
    
    
    // LIGHTBOX
    var lightboxAnimation = new TimelineMax({paused: true, onComplete:lightboxAnimationDone, repeat:0, repeatDelay:0, yoyo:false});
    
    lightboxAnimation.set($(".lightbox"), { display: "block" }, 0),
    
    lightboxAnimation.to('.lightbox', .7, {opacity: 1, ease:Power3.easeOut}, 0);
    
    //lightboxAnimation.set($("body"), { overflow: "hidden" }, 0);
    
    function lightboxAnimationDone(){}

    
    
    
    
    
    // LIGHTBOX
    var lightboxCloseAnimation = new TimelineMax({paused: true, onComplete:lightboxcloseAnimationDone, repeat:0, repeatDelay:0, yoyo:false});
    
    lightboxCloseAnimation.to('.lightbox', .5, {opacity: 0, ease:Power3.easeOut}, 0);

    function lightboxcloseAnimationDone(){
        TweenMax.set($("body"), { overflow: "auto" });
        TweenMax.set($(".lightbox"), { display: "none" });
        $('.lightboxContent').html('');
    }
    
    
    
    
    
    $('.lightbox_link').bind('click',function(event){
        
        event.preventDefault();
        
        //$('.lightbox').css('height', $('.con_site').height());
       
        _path = $(this).attr("rel");
        
        var _isGallery = (_path.indexOf(",")!=0) ? true : false;
        var _isPage = _path.indexOf(".html");
        
        if(_isPage != -1){
            
        }else if(_isGallery){
            
            _gallery = _path.split(",");
            popLightbox(_gallery[0]); 
            
        }else{
           popLightbox(_path); 
        }
        
    });
    
    
    $(document.body).on('click','.thumbnail_lightbox', function(e) { 
        
        var _target = e.target;
        
        var _path = $(_target).attr("rel");
        
        $(_target).css({"border-color": "#1a91ec"}); 
        
        swapImage(_path); 
    });
   
    
    function swapImage(_n){
        
        event.preventDefault();
        
        /*
        _count += 1;
        
        _count = (_count>(_gallery.length-1))? 0 : _count;

        console.log("called link" + _gallery[_count]);
        
        $(".lightboxImg").attr("src", _gallery[_count]);
        
        */

        $(".lightboxImg").attr("src", _n);
        
    }
    
    
    function popLightbox(_n){
        
        lightboxAnimation.restart(); 
        
        var _count = _n.indexOf("mp4");
        
        if(_count != -1){
            
            var _path   = _n.substring(0, (_count - 1));
                          
            $('.lightboxContent').html('<div class="lightboxClose">x</div><video width="996" height="538" controls  poster=""><source src='+_n+' type="video/mp4"><source  src="'+path+'.ogg" type="video/ogg"><source src="'+_path+'.webm" type="video/webm">Your browser does not support the video tag or the file format of this video.</video>');
            
        }else{
            
            var _thumbs = "";
            
            for(i=0;i<_gallery.length;i++){
                var _targetpath = _gallery[i];
                var _classname = (i!=0)? "thumbnail_target" : "thumbnail_first";
                _thumbs += "<div class='thumbnail_lightbox'><div class="+_classname+"  rel="+_targetpath+"></div></div>";
            }
            
            $('.lightboxContent').html('<div class="lightboxClose_con"><div class="lightboxClose">x</div></div><img class="lightboxImg" src='+_n+'><br/><div class="thumbnail_con">'+_thumbs+'</div>');
            
            
            
            $('.lightboxClose').bind('click',function(event){
              closeLightbox();
              event.preventDefault();
            });
        
        }
        
        
        $('.lightboxContent').css("top", $('body').scrollTop()+'px');
         
    }
    
    
    
    
    
             
    
    
    
    
    
    
    
    //
    function closeLightbox(){
        lightboxCloseAnimation.restart();
    }
    
    /*
    window.onscroll = function(event){
        bodyScroll = $('body').scrollTop();
        $('.lightboxContent').css('top', bodyScroll);
        console.log('test light');
    }
    */

});