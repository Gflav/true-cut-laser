<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="laser cutting, steel processing" />
    <meta name="description" content="True Cut Laser provides the expertise to produce custom steel wall panels, intricate decorative shapes and signage or structural components." />
    <meta name="generator" content="" />
    <meta name="CHANNEL" content=""/>
    <meta name="SECTION" content=""/>
    <meta name="SUBSECTION" content=""/>
    <meta name="PAGE" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        
    <title>True Cut Laser - form</title>
    
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="True Cut Laser" />
    <meta property="og:description" content="True Cut provides the expertise to produce custom steel wall panels, intricate decorative shapes and signage or structural components." />
    <meta property="og:url" content="http://truecutlaser.com/" />
    <meta property="og:site_name" content="True Cut Laser" />
    <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"http:\/\/truecutlaser.com\/","name":"True Cut Laser","potentialAction":{"@type":"SearchAction","target":"http:\/\/truecutlaser.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
        
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300,700,900,700i,900i' rel='stylesheet' type='text/css'/>
    
    
    <link rel="stylesheet" href="form.css">
    
    
</head>

    
<body>
    
    
    
    <div class="alpha">
        
        <div class="title"><h1>Get a Quote</h1></div>    
        
        <form id="getaquote" action="form-action.php" method="POST" enctype="multipart/form-data">			
            
        <div class="field_con"><div class="label_con"><div class="label">First Name:</div><div class="required">* Required</div></div>
            
        <div class="input_con"><input class="input"  name="first_name" type="text" value="" size="30" data-required="true"/></div></div>
            
        <div class="field_con"><div class="label_con"><div class="label">Last Name:</div><div class="required">* Required</div></div>
        <div class="input_con"><input class="input"  name="last_name" type="text" value="" size="30" data-required="true"/></div></div>
        
            
            
        <div class="field_con"><div class="label_con"><div class="label">Title:</div><div class="required">* Required</div></div>
        <div class="input_con"><input class="input"  name="title" type="text" value="" size="30" data-required="true"/></div></div>
            
        <div class="field_con"><div class="label_con"><div class="label">Company:</div><div class="required">* Required</div></div>
        <div class="input_con"><input class="input"  name="company" type="text" value="" size="30" data-required="true"/></div></div>
            
            
            
            
        <div class="field_con"><div class="label_con"><div class="label">Contact Email:</div><div class="required">* Required</div></div>
        <div class="input_con"><input class="input"  name="email" type="text" value="" size="30" data-required="true"/></div></div>
            
        
        <div class="field_con"><div class="label_con"><div class="label">Phone Number:</div><div class="required">* Required</div></div>
        <div class="input_con"><input class="input" name="phone" type="text" value="" size="30" data-required="true"/></div></div>
        
        
        <div class="field_con"><div class="label_con"><div class="label">Upload a file:</div></div>
        

        <div class="upload_con">
			<input id="selected_files" class="upload_input" type="text" placeholder="Choose File" disabled="disabled" />
			<div class="upload_icon">
				<input id="file_upload" multiple data-multiple-caption="{count} files selected" class="upload_input hidden" name="file_attach[]" type="file" value="" size="30" accept="application/pdf,image/png,image/jpeg,image/jpg,image/pjpeg" />
				<img src="folder.svg" height="30px"><div class="icon_label">Browse...</div></div></div>
        </div>

		
		
            
        <div class="full_con"><div class="label">Your Message:</div>
        <div class="textarea_con"><textarea name="message" rows="7" cols="30"></textarea></div></div>
            
       
        <div class="button_con"><button type="submit" form="getaquote" value="Submit">Submit for quote</button></div>
            
        </form>
    
	<div id="contact_results"></div>
    </div>
    
    
  
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">
	
document.getElementById("file_upload").onchange = function () {
    document.getElementById("selected_files").value = this.value;
};

var inputs = document.querySelectorAll( '.upload_input.hidden' );
Array.prototype.forEach.call( inputs, function( input ) {
	var label	 = document.getElementById("selected_files"),
		labelVal = label.innerHTML;
		input.addEventListener( 'change', function( e ) {
		var fileName = '';
		if( this.files && this.files.length > 1 )
			fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
		else
			fileName = e.target.value.split( '\\' ).pop();
		if( fileName )
			label.value = fileName;
		else
			label.value = labelVal;
	});
});

$(document).ready(function() {
	$("input").on("focus", function () {
		$(this).removeClass("error");   
	});
});	

// var allowed_file_size = "26214400"; 25mb
// var allowed_file_size = "10485760"; // 10 mb
var allowed_file_size = "2097152"; // 2 mb
var allowed_files = ['image/png', 'application/pdf', 'image/jpeg', 'image/jpg', 'image/pjpeg'];

$("#getaquote").submit(function(e){
    e.preventDefault(); //prevent default action 
	proceed = true;
	
	//simple input validation
	$($(this).find("input[data-required=true], textarea[data-required=true]")).each(function(){
            if(!$.trim($(this).val())){ //if this field is empty 
				$(this).addClass("error");   
                proceed = false; //set do not proceed flag
            }
            //check invalid email
            var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
            if($(this).attr("type")=="email" && !email_reg.test($.trim($(this).val()))){
				$(this).addClass("error");   
                proceed = false; //set do not proceed flag              
            }   
	}).on("input", function(){ //change border color to original
		$(this).removeClass("error");   
	});
	
	//check file size and type before upload, works in modern browsers
	if(window.File && window.FileReader && window.FileList && window.Blob){
		var total_files_size = 0;
		$(this.elements['file_attach[]'].files).each(function(i, ifile){
			if(ifile.value !== ""){ //continue only if file(s) are selected
                if(allowed_files.indexOf(ifile.type) === -1){ //check unsupported file
                    alert( ifile.name + " is unsupported file type!");
                    proceed = false;
                }
             total_files_size = total_files_size + ifile.size; //add file size to total size
			}
		}); 
       if(total_files_size > allowed_file_size){ 
            alert( "Make sure total file size is less than 2 MB!");
            proceed = false;
        }
	}
	
	//if everything's ok, continue with Ajax form submit
	if(proceed){ 
		var post_url = $(this).attr("action"); //get form action url
		var request_method = $(this).attr("method"); //get form GET/POST method
		var form_data = new FormData(this); //Creates new FormData object
		
		$.ajax({ //ajax form submit
			url : post_url,
			type: request_method,
			data : form_data,
			dataType : "json",
			contentType: false,
			cache: false,
			processData:false
		}).done(function(res){ //fetch server "json" messages when done
			if(res.type == "error"){
				$("#contact_results").html('<div class="error">'+ res.text +"</div>");
			}
			
			if(res.type == "done"){
				$("#contact_results").html('<div class="success">'+ res.text +"</div>");
			}
		});
	}
});
</script>

    
</body>
</html>

