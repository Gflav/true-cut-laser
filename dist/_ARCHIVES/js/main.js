$(document).ready(function() 
{
    
    //
    //
    // FOR INTRO
    //
    //
    var introAnimation = new TimelineMax({paused: true, onComplete:introAnimationDone, repeat:0, repeatDelay:0, yoyo:false});
    
    introAnimation.fromTo('.logo_top', .3, {left: -70, autoAlpha:0, ease:Power3.easeOut},{left: 0, autoAlpha:1, ease:Power3.easeOut}, .6)
    .fromTo('.logo_bottom', .4, {left: 70, autoAlpha:0, ease:Power3.easeIn},{left: 0, autoAlpha:1, ease:Power3.easeInOut}, "+=.3")
    .fromTo('.services_content', 1.6, {right: 70, autoAlpha:0, ease:Power3.easeIn},{right: 0, autoAlpha:1, ease:Power3.easeInOut}, "+=.9");
    
    
    function introAnimationDone(){}
    
    
    var _run = false;
    var _hover = false;
    var _offset = (!isMobile())? 40:10;
    var _header_transition_target = $(".lead").height();
    var _header_height_open = 50;
    var _open = false;
    
    TweenMax.set(".header_con", {height:_header_height_open});
    
    
    
    
    window.onscroll = function (event) {
        
        
         
         if($(window).scrollTop() > _header_transition_target){
             
             var _isMobile = isMobile();
             
             if(!_run && !_isMobile){
                TweenMax.set(".header_con", {css:{position:"fixed",top: "0px"}});
                TweenMax.to(".header_con", .2, {height:60,ease:Power0.easeInOut});
                TweenMax.to(".header_background", .2, {css:{opacity:1},ease:Power0.easeInOut});
                TweenMax.to(".logo", .2, {height:35,ease:Power0.easeOut});
                 
                _run = true;
                _hover = true;
             }
        
         }else{
             if(_run){
                TweenMax.set(".header_con", {css:{position:"absolute",top: "12px"}});
                 TweenMax.to(".header_background", .2, {css:{opacity:0},ease:Power0.easeInOut});
                 TweenMax.to(".header_con", .1, {height:_header_height_open,ease:Power0.easeOut});
                 TweenMax.to(".logo", .1, {height:60,ease:Power0.easeOut});
    
                _run = false;
                _hover = false;
             }
         }
         
     }
    
    
    
    
    
    
    
    $('a.logobar').bind('click',function(event){
      TweenMax.to(window, 2, {scrollTo:{y:0}, ease:Power2.easeOut});
      event.preventDefault();
    });
    
  
     
    

    $('.nav_mobile').bind('click',function(event){
        event.preventDefault();
        
        if(!_open){
              open_mobile_nav();
           }else{
              close_mobile_nav();
           }

    });
    
     $('.close').bind('click',function(event){
        event.preventDefault();
        close_mobile_nav();
    });
    
    
    
    
     window.onresize = function (event) {}
    
     window.onload = function (event) {
       init();
     }
    
    
    //$(".thumbnail").hover(over, out);
    //$(".thumbnail_link").hover(over, out);
    
    
    function isMobile(){
        var _v = $(window).width() < 900;
        return _v;
    }
    
    
    
    
    
    $('.jumplink').bind('click',function(event){
        var tarID = "#"+$(this).attr('rel');
        
        if(_open){
            close_mobile_nav();
        }
        
        var tar = ($(tarID).offset().top - _offset);
        TweenMax.to(window, 1, {scrollTo:tar, ease:Power4.easeInOut});
        event.preventDefault();
    });
    
    
    
    function close_mobile_nav(){
         TweenMax.to(mobile_nav, .2, {autoAlpha: 0, ease:Power2.easeOut});
        _open = false;
    }
    
    function open_mobile_nav(){
         TweenMax.to(mobile_nav, .2, {autoAlpha: 1, ease:Power2.easeOut});
        _open = true;
    }
     
    function init(){
        TweenMax.set($(".logo"), { display: "block" });
        introAnimation.restart();
    }
    
    
    
    
    
//END
});
$(document).ready(function() 
{

    
    // LIGHTBOX
    var lightboxAnimation = new TimelineMax({paused: true, onComplete:lightboxAnimationDone, repeat:0, repeatDelay:0, yoyo:false});
    
    lightboxAnimation.set($(".lightbox"), { display: "block" }, 0),
    
    lightboxAnimation.to('.lightbox', .7, {opacity: 1, ease:Power3.easeOut}, 0),
    
    lightboxAnimation.set($("body"), { overflow: "hidden" }, 0);
    
    function lightboxAnimationDone(){}

    
    
    
    
    
    // LIGHTBOX
    var lightboxCloseAnimation = new TimelineMax({paused: true, onComplete:lightboxcloseAnimationDone, repeat:0, repeatDelay:0, yoyo:false});
    
    lightboxCloseAnimation.to('.lightbox', .5, {opacity: 0, ease:Power3.easeOut}, 0);

    function lightboxcloseAnimationDone(){
        TweenMax.set($("body"), { overflow: "auto" });
        TweenMax.set($(".lightbox"), { display: "none" });
        $('.lightboxContent').html('');
    }
    
    
    
    
    
    //
    $('.lightbox').bind('click',function(event){
      closeLightbox();
      event.preventDefault();
    }); 
    
    
    
    $('.thumbnail').bind('click',function(event){
        
        event.preventDefault();
        
        var _path = $(this).attr("href");
        
        //console.log("_path " + _path);
        
        var _isPage = _path.indexOf(".html");
        
        console.log("Thumbnail Clicked " + _isPage);
        
        if(_isPage != -1){
            
        }else{
           popLightbox(_path); 
        }
    });
    
    function popLightbox(_n){
       lightboxAnimation.restart(); 
        
        
        var _count = _n.indexOf("mp4");
        
        if(_count != -1){
            
            var path = _n.substring(0, (_count - 1));
                                    
             console.log(path);
            
            $('.lightboxContent').html('<video width="996" height="538" controls  poster=""><source src='+_n+' type="video/mp4"><source  src="'+path+'.ogg" type="video/ogg"><source src="'+path+'.webm" type="video/webm">Your browser does not support the video tag or the file format of this video.</video>');
        }else{
            $('.lightboxContent').html('<img class="lightboxImg" src='+_n+'>');
        }
        
    }
    //
    function closeLightbox(){
       lightboxCloseAnimation.restart();
    }
    
    

});