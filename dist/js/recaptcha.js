/* start of recaptcha / form code */
	
	
	document.getElementById("file_upload").onchange = function () {
	document.getElementById("selected_files").value = this.value;
	};

	var inputs = document.querySelectorAll( '.upload_input.hidden' );
	Array.prototype.forEach.call( inputs, function( input ) {
		var label	 = document.getElementById("selected_files"),
			labelVal = label.innerHTML;
			input.addEventListener( 'change', function( e ) {
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();
			if( fileName )
				label.value = fileName;
			else
				label.value = labelVal;
		});
	});

	$(document).ready(function() {
		$("input").on("focus", function () {
			$(this).removeClass("error");   
		});
	});	

	// var allowed_file_size = "26214400"; 25mb
	// var allowed_file_size = "10485760"; // 10 mb
	var allowed_file_size = "2097152"; // 2 mb
	// var allowed_files = ['text/plain','image/png', 'application/pdf', 'image/jpeg', 'image/jpg', 'image/pjpeg','image/vnd.dxf', 'application/dxf', 'image/vnd.dwg', 'image/x-dwg'];



	$("#getaquote").submit(function(e){
	    e.preventDefault(); //prevent default action 	
	
	
	    var token =   window.grecaptcha.getResponse(recaptchaId);
	    if (!token) {
			window.grecaptcha.execute(recaptchaId);
	    }


		proceed = true;	
	
		//simple input validation
		$($(this).find("input[data-required=true], textarea[data-required=true]")).each(function(){
	            if(!$.trim($(this).val())){ //if this field is empty 
					$(this).addClass("error");   
	                proceed = false; //set do not proceed flag
	            }
	            //check invalid email
	            var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
	            if($(this).attr("type")=="email" && !email_reg.test($.trim($(this).val()))){
					$(this).addClass("error");   
	                proceed = false; //set do not proceed flag              
	            }   
		}).on("input", function(){ //change border color to original
			$(this).removeClass("error");   
		});
	
		//check file size and type before upload, works in modern browsers
		if(window.File && window.FileReader && window.FileList && window.Blob){
			var total_files_size = 0;
			$(this.elements['file_attach[]'].files).each(function(i, ifile){
				if(ifile.value !== ""){ //continue only if file(s) are selected
	                // if(allowed_files.indexOf(ifile.type) === -1){ //check unsupported file
 // 	                    alert( ifile.name + " is unsupported file type!");
 // 	                    proceed = false;
 // 	                } 
	             total_files_size = total_files_size + ifile.size; //add file size to total size
				}
			}); 
	       if(total_files_size > allowed_file_size){ 
	            alert( "Make sure total file size is less than 2 MB!");
	            proceed = false;
	        }
		}
	
	
		//if everything's ok, continue with Ajax form submit
		if(proceed){ 
			var post_url = $(this).attr("action"); //get form action url
			var request_method = $(this).attr("method"); //get form GET/POST method
			var form_data = new FormData(this); //Creates new FormData object
		
			$.ajax({ //ajax form submit
				url : post_url,
				type: request_method,
				data : form_data,
				dataType : "json",
				contentType: false,
				cache: false,
				processData:false
			}).done(function(res){ //fetch server "json" messages when done
				if(res.type == "error"){
					$("#contact_results").html('<div class="error">'+ res.text +"</div>");
				}
			
				if(res.type == "done"){
					$("#contact_results").html('<div class="success">'+ res.text +"</div>");
				}
			});
		}
	});
/* end of recaptcha / form code */